#include "ObstacleManager.h"
#include "maths.h"

#include <stdlib.h>

void ObstacleManager::addShark(unsigned int lane, float time) {
    for (Shark& shark : sharks) {
        if(!shark.active) {
            shark.active = true;
            shark.lane = lane;
            shark.x = SHARK_SPAWN_X;
            lastGeneratedSharkTime = time;
            break;
        }
    }
}

void ObstacleManager::generateSharks(float time, float dt) {
    if(time - lastGeneratedSharkTime > lerp(0.2,0.01,min(time/90,1))) {
        addShark(rand() % LANE_COUNT, time);
    }
}

void ObstacleManager::updateSharks(float time, float dt) {

    updatePossiblePath(time,dt);
    generateSharks(time,dt);

    for (Shark& shark : sharks) {
        if(shark.active) {
            shark.x -= SHARK_BASE_SPEED * dt;
            if(shark.x < SHARK_KILL_X) {
                killShark(shark);
            }
        }
    }

}

void ObstacleManager::drawSharks(Renderer &renderer, float time) {

    for (Shark& shark : sharks) {
        if(shark.active) {
            SpriteInfo info;
            info.sprite = Renderer::getSprite(SpriteName::SHARK);
            info.posX = roundf(shark.x);
            info.posY = roundf((float)(shark.lane) * LANE_HEIGHT + FIRST_LANE_POSITTION - 1);
            info.transparent = true;
            renderer.addSprite(info);
        }
    }
    possiblePath.draw(renderer);

}

void ObstacleManager::checkCollisions(Player &player) {
    for (Shark& shark : sharks) {
        if (shark.active) {
            Collider sharkCollider = shark.getCollider();
            if(possiblePath.collides(sharkCollider)) {
                killShark(shark);
            }
            if(player.collides(sharkCollider)) {
                player.dead = true;
            }
        }
    }
}

void ObstacleManager::clear() {
    for (Shark& shark : sharks) {
        shark.active = false;
    }
    lastGeneratedSharkTime = 0;
    timeOfNextMove = 0;
}

ObstacleManager::ObstacleManager() : possiblePath(SCREEN_WIDTH,1,4) {

}

void ObstacleManager::updatePossiblePath(float time, float dt) {
    if (time > timeOfNextMove) {
        timeOfNextMove = time + lerp(0.3,0.3,(rand() % 100) / 100.0);

        if(possiblePath.wantLane == 0) {
            possiblePath.update(1, dt);
        } else if (possiblePath.wantLane == LANE_COUNT - 1) {
            possiblePath.update(-1, dt);
        } else {
            int direction = (rand() % 2) * 2 - 1;
            possiblePath.update(direction, dt);
        }
    }
    possiblePath.update(0, dt);
}

void ObstacleManager::killShark(Shark& shark) {
    shark.active = false;
}

Collider Shark::getCollider() const {
    return Collider(x, getY(), 1, 3, 4, 1);
}

float Shark::getY() const{
    return (float)lane * LANE_HEIGHT + FIRST_LANE_POSITTION - 1;
}
