#pragma once

#include "Collider.h"
#include "InputManager.h"
#include "Graphics/Renderer.h"

class Player : public Collider {
public:
    bool dead = false;

    float speedY = 15;
    int wantLane = 0;

    Player();
    Player(float x, float hitboxYOffset, float hitboxHeight);
    void updateWithInputs(InputManager& inputs, float dt);
    void update(int direction, float dt);
    void draw(Renderer& renderer);
};

