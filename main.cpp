#define _WIN32_WINNT 0x0501
#pragma execution_character_set( "utf-8" )
#include <windows.h>
#include <iostream>
#include "Graphics/Renderer.h"
#include "Game.h"


void setup() {
    /*The function does not clear the last error information. if last value was zero.*/
    SetLastError(NO_ERROR);

    HWND hwnd_console = GetConsoleWindow();
    HANDLE hOutput = GetStdHandle( STD_OUTPUT_HANDLE );

    SetWindowLongPtr(hwnd_console,GWL_STYLE,0);

    SMALL_RECT windowRect;
    windowRect.Top = 0;
    windowRect.Left = 0;
    windowRect.Right = SCREEN_WIDTH - 1;
    windowRect.Bottom = SCREEN_HEIGHT - 1;

    COORD bufferSize;
    bufferSize.X = SCREEN_WIDTH;
    bufferSize.Y = SCREEN_HEIGHT;

    SetConsoleScreenBufferSize(hOutput, bufferSize);

    SetConsoleWindowInfo(hOutput, true, &windowRect);
    SetConsoleOutputCP( 65001 );


    SetWindowLongPtr(hwnd_console,GWL_STYLE, WS_OVERLAPPED     |
                                             WS_CAPTION        |
                                             WS_SYSMENU        |
                                             WS_MINIMIZEBOX);

    //show window after updating
    ShowWindow(hwnd_console,SW_SHOW);
}

int main(int argc, char ** argv)
{
    setup();

    Game game;

    game.loop();

    return 0;
}