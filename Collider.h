#pragma once

class Collider {
public:
    float posX;
    float posY;

    float hitboxXOffset;
    float hitboxYOffset;
    float hitboxWidth;
    float hitboxHeight;

    Collider(float x, float y, float hitboxXOffset, float hitboxYOffset, float width, float height);
    bool collides(const Collider& collider) const;
};