#include "Game.h"

#include <windows.h>

void Game::loop() {
    timer = NYTimer();
    timer.start();

    HANDLE hOutput = GetStdHandle( STD_OUTPUT_HANDLE );
    COORD dwBufferSize = { SCREEN_WIDTH,SCREEN_HEIGHT };
    COORD dwBufferCoord = { 0, 0 };
    SMALL_RECT rcRegion = { 0, 0, SCREEN_WIDTH-1, SCREEN_HEIGHT-1 };

    while (!closeGame) {
        float dt = timer.getElapsedSeconds(true);

        inputs.updateInputs();

        if(inputs.isPressed(InputManager::Key::ESCAPE)) {
            closeGame = true;
        }

        ReadConsoleOutputW( hOutput, (CHAR_INFO *)*renderer.screenBuffer, dwBufferSize,
                           dwBufferCoord, &rcRegion );

        if( currentScene == TITLE ) {
            time+=dt;
            renderer.renderTitle(time);
            if(inputs.isPressed(InputManager::Key::ENTER)) {
                 initGame();
            }
        }

        else if( currentScene == GAME ) {
            time+=dt;
            player.updateWithInputs(inputs, dt);
            // Update les obstacles
            obstacles.updateSharks(time, dt);
            obstacles.checkCollisions(player);
            obstacles.drawSharks(renderer, time);
            player.draw(renderer);
            if (player.dead) {
                currentScene = GAME_OVER;
            }
            renderer.render(time);
        }

        else if ( currentScene == GAME_OVER ) {
            obstacles.drawSharks(renderer, time);
            player.draw(renderer);
            renderer.renderGameOver(time);
            if(inputs.isPressed(InputManager::Key::ENTER)) {
                initGame();
            }
        }

        WriteConsoleOutputW( hOutput, (CHAR_INFO *)*renderer.screenBuffer, dwBufferSize,
                            dwBufferCoord, &rcRegion );
    }
}

void Game::initGame() {
    player = Player{};
    time = 0;
    obstacles.clear();
    currentScene = GAME;
}
