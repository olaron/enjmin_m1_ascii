#pragma once

constexpr int SCREEN_WIDTH = 150;
constexpr int SCREEN_HEIGHT = 30;
constexpr int LANE_COUNT = 5;
constexpr int LANE_HEIGHT = 4;

constexpr int MAP_LOWER_BOUND = 1;
constexpr int MAP_HIGHER_BOUND = 24;
constexpr int MAP_BOUND_PADDING = 2;
constexpr int FIRST_LANE_POSITTION = MAP_LOWER_BOUND+MAP_BOUND_PADDING;