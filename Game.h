#pragma once

#include "Player.h"
#include "InputManager.h"
#include "Graphics/Renderer.h"
#include "NYTimer.h"
#include "ObstacleManager.h"

class Game {
public:
    void loop();

private:
    enum Scene {
        TITLE,
        GAME,
        GAME_OVER,
    };

    Scene currentScene = TITLE;

    Player player;
    InputManager inputs;
    Renderer renderer;
    NYTimer timer;
    ObstacleManager obstacles;
    float time = 0;
    bool closeGame = false;

    void initGame();
};

