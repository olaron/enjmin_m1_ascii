#pragma once

#include <windows.h>

constexpr unsigned int INPUT_BUFFER_SIZE = 16;

class InputManager {
public:
    enum Key{UP, DOWN, ENTER, ESCAPE, SIZE};

private:
    HANDLE hInput;
    INPUT_RECORD inputBuffer[INPUT_BUFFER_SIZE];


    bool keyDown[Key::SIZE];
    bool keyPressed[Key::SIZE];

    void resetPressed();
    void updateKey(KEY_EVENT_RECORD keyEvent);

public:
    InputManager();
    void updateInputs();
    bool isPressed(Key key);
    bool isDown(Key key);
};
