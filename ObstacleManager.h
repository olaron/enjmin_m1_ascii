#pragma once

#include "Constants.h"
#include "Graphics/Renderer.h"
#include "Player.h"

constexpr unsigned int SHARK_POOL_SIZE = 100;
constexpr float SHARK_BASE_SPEED = 100;
constexpr float SHARK_SPAWN_X = SCREEN_WIDTH + 5;
constexpr float SHARK_KILL_X = -5;

class Shark {
public:
    unsigned int lane = 0;
    float x = 0.0f;
    bool active = false;

    float getY() const;
    Collider getCollider() const;
};

class ObstacleManager {
public:
    Player possiblePath;
    float timeOfNextMove = 0;

    Shark sharks[SHARK_POOL_SIZE];
    float lastGeneratedSharkTime = 0;

    ObstacleManager();

    void updateSharks(float time, float dt);
    void drawSharks(Renderer& renderer, float time);

    void checkCollisions(Player &player);

    void clear();

    void addShark(unsigned int lane, float time);
    void generateSharks(float time, float dt);
    void updatePossiblePath(float time, float dt);
    void killShark(Shark& shark);
};

