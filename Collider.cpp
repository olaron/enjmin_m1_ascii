#include "Collider.h"

bool Collider::collides(const Collider &collider) const {
    float x = posX + hitboxXOffset;
    float y = posY + hitboxYOffset;

    float cx = collider.posX + collider.hitboxXOffset;
    float cy = collider.posY + collider.hitboxYOffset;

    return (x < cx + collider.hitboxWidth &&
            x + hitboxWidth > cx &&
            y < cy + collider.hitboxHeight &&
            y + hitboxHeight > cy);
}

Collider::Collider(float x, float y, float hitboxXOffset, float hitboxYOffset, float width, float height):
        posX(x),
        posY(y),
        hitboxXOffset(hitboxXOffset),
        hitboxYOffset(hitboxYOffset),
        hitboxWidth(width),
        hitboxHeight(height) {}
