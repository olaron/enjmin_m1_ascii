#include "Player.h"

constexpr unsigned int PLAYER_START_Y = FIRST_LANE_POSITTION - 1;

Player::Player() :
        Collider(2, PLAYER_START_Y,1, 3,1, 1)
{}

Player::Player(float x, float hitboxYOffset, float hitboxHeight) :
    Collider(x,PLAYER_START_Y,1,hitboxYOffset,1,hitboxHeight)
{}


void Player::updateWithInputs(InputManager& inputs, float dt) {

    int direction = 0;
    if(inputs.isPressed(InputManager::Key::DOWN)){
        direction = 1;
    }

    if(inputs.isPressed(InputManager::Key::UP)){
        direction = -1;
    }

    update(direction, dt);
}


void Player::draw(Renderer &renderer) {
    SpriteInfo info;
    info.sprite = Renderer::getSprite(SpriteName::PLAYER);
    info.posX = (int) roundf(posX);
    info.posY = (int) roundf(posY);
    info.transparent = true;
    if(dead) {
        info.colorOverride = textColors[TextColor::RED];
    } else {
        info.colorOverride = textColors[TextColor::GRAY];
    }
    renderer.addSprite(info);
}

void Player::update(int direction, float dt) {

    wantLane += direction;
    if (wantLane >= LANE_COUNT) {
        wantLane = LANE_COUNT - 1;
    }
    if (wantLane < 0) {
        wantLane = 0;
    }

    float wantY = (float) wantLane * LANE_HEIGHT + PLAYER_START_Y;

    float speed = (wantY - posY) * speedY * dt;

    if((posY < wantY && posY + speed > wantY) || (posY > wantY && posY + speed < wantY)) {
        posY = wantY;
    } else {
        posY += speed;
    }
}

