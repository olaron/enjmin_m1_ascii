#pragma once

#include "Sprite.h"
#include "Colors.h"

#include <windows.h>
#include <memory>
#include <string>
#include <vector>


class SpriteCollection {
public:
    SpriteCollection();

    SpriteCollection(SpriteCollection &other) = delete;

    void operator=(const SpriteCollection &) = delete;

    const Sprite *const getSprite(SpriteName name) const;

protected:
    Sprite sprites[SpriteName::SPRITES_COUNT];

    void registerSprites(SpriteName name, Sprite sprite); //Enregistrement au tableau

    void generateSprites();
};

const SpriteCollection g_Collection{};
