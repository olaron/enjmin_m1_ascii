#include "UIHelper.h"

void collectDigits(std::vector<int>& digits, unsigned long num) {
    if (num > 9) {
        collectDigits(digits, num / 10);
    }
    digits.push_back(num % 10);
}

const Sprite * intToSprite(int digit) {
    constexpr static SpriteName NumberNames[10] =
            {SpriteName::UI_0,
             SpriteName::UI_1,
             SpriteName::UI_2,
             SpriteName::UI_3,
             SpriteName::UI_4,
             SpriteName::UI_5,
             SpriteName::UI_6,
             SpriteName::UI_7,
             SpriteName::UI_8,
             SpriteName::UI_9};

    if (digit >= 0 && digit < 10){
        return g_Collection.getSprite(NumberNames[digit]);
        }
    else {
        return nullptr;
    }
}

std::vector<Sprite *> UIHelper::getScoreSprite(int score) {
    std::vector<int> digits;
    collectDigits(digits, score);

    std::vector<Sprite *> sprites;
    sprites.reserve(digits.size());
    for (int digit : digits) {
        sprites.push_back(const_cast<Sprite *>(intToSprite(digit)));
    }

    return sprites;
}

