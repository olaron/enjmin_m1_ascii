#pragma once

enum TextColor {
    RED,
    LIGHT_BLUE,
    GRAY,
    WHITE,
    BACKBLUE,
    BACKLIGHT_BLUE,
    BACKDARKGRAY,
    COLORSCOUNT
};

static constexpr WORD textColors[TextColor::COLORSCOUNT] {0x0004, //RED
                                                          0x0001 | 0x0008, //LIGHT BLUE
                                                          0x0004|0x0002|0x0001, //GRAY
                                                          0x0004|0x0002|0x0001|0x0008, // WHITE
                                                          0x0010, //BACK BLUE
                                                          0x0010 | 0x0080, //BACK LIGHT BLUE
                                                          0x0000 | 0x0080, //BACK DARK GRAY
};