#pragma once

#include <vector>
#include <windows.h>

enum SpriteName {SHARK, PLAYER, UI_0, UI_1, UI_2, UI_3, UI_4, UI_5, UI_6, UI_7, UI_8, UI_9, SPRITES_COUNT};


struct Sprite{
public:
    int height;
    int width;
    std::vector<CHAR_INFO> chars;

};

struct SpriteInfo{
public:
    int posX = 0;
    int posY = 0;
    Sprite * sprite = nullptr;
    bool transparent = false;
    WORD colorOverride = 0b0000;
};

