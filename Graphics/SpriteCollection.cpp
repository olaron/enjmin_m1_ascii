#include "SpriteCollection.h"

SpriteCollection::SpriteCollection() {
    generateSprites();
}


std::vector<CHAR_INFO> GetBufferFromString(std::string s, int width){
    std::vector<CHAR_INFO>  buff(s.size());

    int j = 0;
    for (int i = 0; i < s.size(); i++){
        if (s[i] == '\n'){
            continue;
        }

        int y = j/width;
        int x = j-(y*width);
        buff[x*width+y].Char.UnicodeChar = s[i];
        j++;
    }

    return buff;
}

void SpriteCollection::generateSprites() {
    {
        // Shark
        Sprite sprite;
        sprite.height = 3;
        std::vector<CHAR_INFO> buffer(15);
        buffer[0].Char.UnicodeChar = 0x0;
        buffer[0].Attributes = 0b0000;
        buffer[1].Char.UnicodeChar = 0x0;
        buffer[1].Attributes = 0b0000;
        buffer[2].Char.UnicodeChar = '/';
        buffer[2].Attributes = textColors[TextColor::BACKDARKGRAY] | textColors[TextColor::WHITE];

        buffer[3].Char.UnicodeChar = 0x0;
        buffer[3].Attributes = 0b0000;
        buffer[4].Char.UnicodeChar = '.';
        buffer[4].Attributes = textColors[TextColor::BACKDARKGRAY] | textColors[TextColor::WHITE];
        buffer[5].Char.UnicodeChar = ' ';
        buffer[5].Attributes = textColors[TextColor::BACKDARKGRAY] | textColors[TextColor::WHITE];

        buffer[6].Char.UnicodeChar = 0x0;
        buffer[6].Attributes = 0b0000;
        buffer[7].Char.UnicodeChar = L'\u00B4';
        buffer[7].Attributes = textColors[TextColor::BACKDARKGRAY] | textColors[TextColor::WHITE];
        buffer[8].Char.UnicodeChar = ' ';
        buffer[8].Attributes = textColors[TextColor::BACKDARKGRAY] | textColors[TextColor::WHITE];

        buffer[9].Char.UnicodeChar = L'\u00B4';
        buffer[9].Attributes = textColors[TextColor::BACKDARKGRAY] | textColors[TextColor::WHITE];
        buffer[10].Char.UnicodeChar = '/';
        buffer[10].Attributes = textColors[TextColor::BACKDARKGRAY] | textColors[TextColor::WHITE];
        buffer[11].Char.UnicodeChar = '|';
        buffer[11].Attributes = textColors[TextColor::BACKDARKGRAY] | textColors[TextColor::WHITE];

        buffer[12].Char.UnicodeChar = L'\u00B4';
        buffer[12].Attributes = textColors[TextColor::BACKDARKGRAY] | textColors[TextColor::WHITE];
        buffer[13].Char.UnicodeChar = 0x0;
        buffer[13].Attributes = 0b0000;
        buffer[14].Char.UnicodeChar = 0x0;
        buffer[14].Attributes = 0b0000;


        sprite.chars = buffer;

        registerSprites(SpriteName::SHARK, sprite);
    }

    {
        //Player
        Sprite sprite;
        sprite.height = 4;
        std::vector<CHAR_INFO> buffer(24);

        buffer[0].Char.UnicodeChar = '.';
        buffer[1].Char.UnicodeChar = 0x0;
        buffer[2].Char.UnicodeChar = 0x0;
        buffer[3].Char.UnicodeChar = '_';

        buffer[4].Char.UnicodeChar = '_';
        buffer[5].Char.UnicodeChar = 0x0;
        buffer[6].Char.UnicodeChar = 0x0;
        buffer[7].Char.UnicodeChar = '/';

        buffer[8].Char.UnicodeChar = '_';
        buffer[9].Char.UnicodeChar = '/';
        buffer[10].Char.UnicodeChar = '|';
        buffer[11].Char.UnicodeChar = '_';

        buffer[12].Char.UnicodeChar = 'O';
        buffer[13].Char.UnicodeChar = '/';
        buffer[14].Char.UnicodeChar = '\\';
        buffer[15].Char.UnicodeChar = '/';

        buffer[16].Char.UnicodeChar = 0x0;
        buffer[17].Char.UnicodeChar = '`';
        buffer[18].Char.UnicodeChar = 0x0;
        buffer[19].Char.UnicodeChar = '_';

        buffer[20].Char.UnicodeChar = 0x0;
        buffer[21].Char.UnicodeChar = '-';
        buffer[22].Char.UnicodeChar = 0x0;
        buffer[23].Char.UnicodeChar = '_';

        sprite.chars = buffer;
        registerSprites(SpriteName::PLAYER, sprite);
    }

    //Sprite UI_0
    {
        Sprite sprite;
        sprite.height = 5;
        sprite.width = 5;
        sprite.chars = GetBufferFromString("00000\n"
                                           "0   0\n"
                                           "0   0\n"
                                           "0   0\n"
                                           "00000", 5);
        registerSprites(SpriteName::UI_0, sprite);
    }

    //Sprite UI_1
    {
        Sprite sprite;
        sprite.height = 5;
        sprite.width = 5;
        sprite.chars = GetBufferFromString(" 1111\n"
                                           "11 11\n"
                                           "   11\n"
                                           "   11\n"
                                           "   11", 5);
        registerSprites(SpriteName::UI_1, sprite);
    }

    //Sprite UI_2
    {
        Sprite sprite;
        sprite.height = 5;
        sprite.width = 5;
        sprite.chars = GetBufferFromString("22222\n"
                                           "   22\n"
                                           "22222\n"
                                           "22   \n"
                                           "22222", 5);
        registerSprites(SpriteName::UI_2, sprite);
    }

    //Sprite UI_3
    {
        Sprite sprite;
        sprite.height = 5;
        sprite.width = 5;
        sprite.chars = GetBufferFromString("33333\n"
                                           "   33\n"
                                           "33333\n"
                                           "   33\n"
                                           "33333", 5);
        registerSprites(SpriteName::UI_3, sprite);
    }

    //Sprite UI_4
    {
        Sprite sprite;
        sprite.height = 5;
        sprite.width = 5;
        sprite.chars = GetBufferFromString("4   4\n"
                                           "4   4\n"
                                           "44444\n"
                                           "    4\n"
                                           "    4", 5);
        registerSprites(SpriteName::UI_4, sprite);
    }

    //Sprite UI_5
    {
        Sprite sprite;
        sprite.height = 5;
        sprite.width = 5;
        sprite.chars = GetBufferFromString("55555\n"
                                           "55   \n"
                                           "55555\n"
                                           "   55\n"
                                           "55555", 5);
        registerSprites(SpriteName::UI_5, sprite);
    }

    //Sprite UI_6
    {
        Sprite sprite;
        sprite.height = 5;
        sprite.width = 5;
        sprite.chars = GetBufferFromString("66666\n"
                                           "66   \n"
                                           "66666\n"
                                           "66 66\n"
                                           "66666", 5);
        registerSprites(SpriteName::UI_6, sprite);
    }

    //Sprite UI_7
    {
        Sprite sprite;
        sprite.height = 5;
        sprite.width = 5;
        sprite.chars = GetBufferFromString("77777\n"
                                           "   77\n"
                                           "  77 \n"
                                           " 77  \n"
                                           "77   ", 5);
        registerSprites(SpriteName::UI_7, sprite);
    }

    //Sprite UI_8
    {
        Sprite sprite;
        sprite.height = 5;
        sprite.width = 5;
        sprite.chars = GetBufferFromString("88888\n"
                                           "88 88\n"
                                           "88888\n"
                                           "88 88\n"
                                           "88888", 5);
        registerSprites(SpriteName::UI_8, sprite);
    }

    //Sprite UI_9
    {
        Sprite sprite;
        sprite.height = 5;
        sprite.width = 5;

        sprite.chars = GetBufferFromString("99999\n"
                                           "99 99\n"
                                           "99999\n"
                                           "   99\n"
                                           "99999", 5);
        registerSprites(SpriteName::UI_9, sprite);
    }

}


void SpriteCollection::registerSprites(SpriteName name, Sprite sprite) {
    sprites[name] = std::move(sprite);
}

const Sprite *const SpriteCollection::getSprite(SpriteName name) const {
    return  &sprites[name];
}


