#pragma once

#include "Sprite.h"
#include "SpriteCollection.h"

#include <vector>

class UIHelper {
public:
    static std::vector<Sprite*> getScoreSprite(int score);
};