#include "Renderer.h"

#include <windows.h>
#include <vector>
#include <string>

Renderer::Renderer() {
    perlin.reseed(86796);
}


void Renderer::addSprite(SpriteInfo sprite) {
    spriteToRender.push(sprite);
}

void Renderer::drawSprites() {
    while (!spriteToRender.empty()) {
        SpriteInfo info = spriteToRender.front();
        int xOff = info.posX;
        int yOff = info.posY;

        const Sprite sprite = *info.sprite;
        for (int j = 0; j < sprite.chars.size(); ++j) {
            int x = xOff + j / sprite.height;
            int y = yOff + j % sprite.height;
            if (x < 0 || y < 0 || x >= SCREEN_WIDTH || y >= SCREEN_HEIGHT) {
                continue;
            }

            CHAR_INFO c = sprite.chars.at(j);
            if (info.colorOverride != 0x0000) {
                c.Attributes = info.colorOverride;
            }

            if (c.Char.UnicodeChar != 0x0 || c.Char.AsciiChar != 0x0) {

                if (info.transparent && (c.Attributes & 0b11110000) == 0) {
                    unsigned int backColor = screenBuffer[y][x].Attributes & 0b11110000;
                    unsigned int frontColor = c.Attributes & 0b1111;
                    unsigned int color = backColor | frontColor;

                    screenBuffer[y][x] = c;
                    screenBuffer[y][x].Attributes = color;
                } else {
                    screenBuffer[y][x] = c;
                }


            }
        }

        spriteToRender.pop();
    }
}

void Renderer::drawBackground(float time) {

    for (int x = 0; x < SCREEN_WIDTH; ++x) {
        for (int y = 0; y < SCREEN_HEIGHT; ++y) {
            if (screenBuffer[y][x].Char.UnicodeChar & 0xFF00) {
                screenBuffer[y][x].Char.AsciiChar = ' ';
            } else {
                screenBuffer[y][x].Char.UnicodeChar = ' ';
            }
            screenBuffer[y][x].Attributes = 0x00;

            float perlinValue = perlin.noise2D_0_1((time * 3) + (double) x / SCREEN_WIDTH * 10,
                                                   (double) y / SCREEN_HEIGHT * 0.1);

            if (y >= MAP_LOWER_BOUND && y <= MAP_HIGHER_BOUND) {
                if (y < MAP_LOWER_BOUND + MAP_BOUND_PADDING || y > MAP_HIGHER_BOUND - MAP_BOUND_PADDING) {
                    screenBuffer[y][x].Char.UnicodeChar = '_';
                    screenBuffer[y][x].Attributes =
                            textColors[TextColor::WHITE] | textColors[TextColor::BACKLIGHT_BLUE];

                } else if ((y + MAP_BOUND_PADDING) % 4 == 0) {
                    screenBuffer[y][x].Char.UnicodeChar = '_';
                    screenBuffer[y][x].Attributes = textColors[TextColor::WHITE] | textColors[TextColor::BACKBLUE];
                } else {
                    float val = sin((time * 30 + x + (y * y * 15)));
                    if (val > 0.7) {
                        if (val > 0.8) {
                            if (perlinValue > 0.5) {
                                screenBuffer[y][x].Char.UnicodeChar = L'\u0336';
                                screenBuffer[y][x].Attributes = textColors[TextColor::LIGHT_BLUE];
                            } else {
                                if (screenBuffer[y][x].Char.UnicodeChar & 0xFF00) {
                                    screenBuffer[y][x].Char.UnicodeChar = L'\u033D';
                                } else {
                                    screenBuffer[y][x].Char.UnicodeChar = L'\u003D';
                                }
                                screenBuffer[y][x].Attributes = textColors[TextColor::WHITE];
                            }

                        } else {
                            if (screenBuffer[y][x].Char.UnicodeChar & 0xFF00) {
                                screenBuffer[y][x].Char.UnicodeChar = L'\u032D';
                            } else {
                                screenBuffer[y][x].Char.UnicodeChar = L'\u002D';
                            }
                            screenBuffer[y][x].Attributes = textColors[TextColor::GRAY];
                        }
                    }
                    WORD w = textColors[TextColor::BACKBLUE];
                    screenBuffer[y][x].Attributes |= w;
                }

            }

        }

    }
}


void Renderer::render(float time) {
    drawBackground(time);
    renderScore(time, 75, 25);
    drawSprites();
}

void Renderer::renderTitle(float time) {
    drawBackground(time);
    const std::string title = "SharkSurf";
    drawText(title, SCREEN_WIDTH / 2 - title.length() / 2, (SCREEN_HEIGHT / 2) - 5, TextColor::WHITE);

    const std::string line1 = "Press UP/DOWN to change lanes";
    drawText(line1, SCREEN_WIDTH / 2 - line1.length() / 2, (SCREEN_HEIGHT / 2) - 3, TextColor::WHITE);
    const std::string line2 = "Press ENTER to start";
    drawText(line2, SCREEN_WIDTH / 2 - line2.length() / 2, (SCREEN_HEIGHT / 2) - 2, TextColor::WHITE);
    const std::string line3 = "Press ESC to quit";
    drawText(line3, SCREEN_WIDTH / 2 - line3.length() / 2, (SCREEN_HEIGHT / 2) - 1, TextColor::WHITE);

    const std::string line4 = "Avoid the sharks!";
    drawText(line4, SCREEN_WIDTH / 2 - line4.length() / 2, (SCREEN_HEIGHT / 2) + 1, TextColor::WHITE);
}

void Renderer::drawText(const std::string &text, unsigned int x, unsigned int y, TextColor color) {
    for (int i = 0; i < text.length(); ++i) {
        screenBuffer[y][x + i].Char.UnicodeChar = text[i];
        screenBuffer[y][x + i].Attributes = textColors[color];
    }
}

void Renderer::renderGameOver(float time) {
    drawBackground(time);
    renderScore(time, 75, 25);
    drawSprites();

    std::string title = "GAME OVER";
    drawText(title, SCREEN_WIDTH / 2 - title.length() / 2, (SCREEN_HEIGHT / 2) - 4, TextColor::RED);

    const std::string line2 = "Press ENTER to restart";
    drawText(line2, SCREEN_WIDTH / 2 - line2.length() / 2, (SCREEN_HEIGHT / 2) - 2, TextColor::RED);

}


Sprite *Renderer::getSprite(SpriteName name) {
    return const_cast<Sprite *>(g_Collection.getSprite(name));
}

void Renderer::renderScore(float score, unsigned int x, unsigned int y) {
    std::vector<Sprite *> digits = UIHelper::getScoreSprite((int) score);

    for (int i = 0; i < digits.size(); ++i) {
        SpriteInfo info;
        info.posX = x + (digits[i]->width * i * 2);
        info.posY = y;
        info.sprite = digits[i];
        info.transparent = true;
        info.colorOverride = textColors[TextColor::WHITE];
        addSprite(info);
    }
}
