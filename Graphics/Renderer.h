#pragma once

#include <stack>
#include <string>
#include <queue>
#include <map>

#include "../Constants.h"
#include "PerlinNoise.h"
#include "Sprite.h"
#include "Colors.h"
#include "SpriteCollection.h"
#include "UIHelper.h"


class Renderer {
public:
    Renderer();

    CHAR_INFO screenBuffer[SCREEN_HEIGHT][SCREEN_WIDTH];

    static Sprite *getSprite(SpriteName name);

    void addSprite(SpriteInfo sprite); // Ajoute un sprite a l'écran

    void drawSprites(); // Draw tout les sprites

    void drawBackground(float time);

    void render(float time);

    void renderTitle(float time);

    void renderScore(float score, unsigned int x, unsigned int y);

    void renderGameOver(float time);

private:
    std::queue<SpriteInfo> spriteToRender;

    siv::PerlinNoise perlin;

    void drawText(const std::string &text, unsigned int x, unsigned int y, TextColor color);
};



