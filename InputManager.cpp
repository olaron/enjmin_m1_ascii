#include "InputManager.h"

#include <stdexcept>

InputManager::InputManager() {
    hInput = GetStdHandle(STD_INPUT_HANDLE);
    for (int i = 0; i < Key::SIZE; ++i) {
        keyPressed[i] = false;
        keyDown[i] = false;
    }
}

void InputManager::updateInputs() {
    resetPressed();
    DWORD cNumRead;

    DWORD nbOfEvents;
    GetNumberOfConsoleInputEvents(hInput,&nbOfEvents);

    if(nbOfEvents == 0) return;

    if (! ReadConsoleInput(
            hInput,      // input buffer handle
            inputBuffer,     // buffer to read into
            INPUT_BUFFER_SIZE,         // size of read buffer
            &cNumRead) ) // number of records read
        throw std::runtime_error("Error while reading inputs");

    for (unsigned int i = 0; i < cNumRead; i++)
    {
        switch(inputBuffer[i].EventType)
        {
            case KEY_EVENT: // keyboard input
                updateKey(inputBuffer[i].Event.KeyEvent);
                break;

            case MOUSE_EVENT: // mouse input
            case WINDOW_BUFFER_SIZE_EVENT: // scrn buf. resizing
            case FOCUS_EVENT:  // disregard focus events
            case MENU_EVENT:   // disregard menu events
            default:
                break;
        }
    }
}

void InputManager::updateKey(KEY_EVENT_RECORD keyEvent) {
    switch (keyEvent.wVirtualKeyCode) {
        case VK_DOWN:
            keyPressed[Key::DOWN] = keyEvent.bKeyDown;
            keyDown[Key::DOWN] = keyEvent.bKeyDown;
            break;
        case VK_UP:
            keyPressed[Key::UP] = keyEvent.bKeyDown;
            keyDown[Key::UP] = keyEvent.bKeyDown;
            break;
        case VK_RETURN:
            keyPressed[Key::ENTER] = keyEvent.bKeyDown;
            keyDown[Key::ENTER] = keyEvent.bKeyDown;
            break;
        case VK_ESCAPE:
            keyPressed[Key::ESCAPE] = keyEvent.bKeyDown;
            keyDown[Key::ESCAPE] = keyEvent.bKeyDown;
            break;
    }

}

void InputManager::resetPressed() {
    for (bool & i : keyPressed) {
        i = false;
    }
}

bool InputManager::isPressed(InputManager::Key key) {
    return keyPressed[key];
}

bool InputManager::isDown(InputManager::Key key) {
    return keyDown[key];
}
